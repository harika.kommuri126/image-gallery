import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {
  private router;

  constructor(router: Router) {
    this.router = router;
   }

  ngOnInit(): void {
  }

  goToDashboard(): void {
    this.router.navigate(['dashboard']);
  }

}

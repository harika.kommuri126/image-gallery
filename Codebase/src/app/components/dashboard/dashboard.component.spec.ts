import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { constants } from '../../constants';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  const mockImagesJson = [
    {
      id: '1006',
      author: 'Vladimir Kudinov',
      width: 3000,
      height: 2000,
      url: 'https://unsplash.com/photos/-wWRHIUklxM',
      download_url: 'https://picsum.photos/id/1006/3000/2000'
    },
    {
      id: '1008',
      author: 'Benjamin Combs',
      width: 5616,
      height: 3744,
      url: 'https://unsplash.com/photos/5L4XAgMSno0',
      download_url: 'https://picsum.photos/id/1008/5616/3744'
    },
    {
      id:  '1009',
      author: 'Christopher Campbell',
      width: 5000,
      height: 7502,
      url: 'https://unsplash.com/photos/CMWRIzyMKZk',
      download_url: 'https://picsum.photos/id/1009/5000/7502'
    }
  ];

  const formatedImagesJson: any = [
    {
      id: '1006',
      author: 'Vladimir Kudinov',
      width: 3000,
      height: 2000,
      url: 'https://unsplash.com/photos/-wWRHIUklxM',
      download_url: 'https://picsum.photos/id/1006/3000/2000',
      imageUrl: 'https://picsum.photos/id/1006/250/200'
    },
    {
      id: '1008',
      author: 'Benjamin Combs',
      width: 5616,
      height: 3744,
      url: 'https://unsplash.com/photos/5L4XAgMSno0',
      download_url: 'https://picsum.photos/id/1008/5616/3744',
      imageUrl: 'https://picsum.photos/id/1008/250/200'
    },
    {
      id:  '1009',
      author: 'Christopher Campbell',
      width: 5000,
      height: 7502,
      url: 'https://unsplash.com/photos/CMWRIzyMKZk',
      download_url: 'https://picsum.photos/id/1009/5000/7502',
      imageUrl: 'https://picsum.photos/id/1009/250/200'
    }
  ];


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [ DashboardComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open image dialog', () => {
    component.openImage(constants.image_url);
    expect(component.selectedImageUrl).toBe(constants.image_url);
  });

  it('should clear image property when clicked on close', () => {
    component.closeDialog();
    expect(component.selectedImageUrl).toBe('');
  });

  it('should change page and limit value when user clicks on page number or change limit', () => {
    spyOn(component, 'loadAvailableImages').and.callThrough();
    component.changeLimit();
    expect(component.loadAvailableImages).toHaveBeenCalled();
  });

  it('should format all imgaes with size 250*200', () => {
    component.formatImages(mockImagesJson);
    expect(component.availableImages).toEqual(formatedImagesJson);
  });
});

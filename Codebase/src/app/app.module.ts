import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CardComponent } from './containers/card-component/card-component.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ImageContainerComponent } from './containers/image-container/image-container.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { MyHttpInterceptor } from './http.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CardComponent,
    PageNotFoundComponent,
    ImageContainerComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

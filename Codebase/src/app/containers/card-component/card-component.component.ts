import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-card-component',
  templateUrl: './card-component.component.html',
  styleUrls: ['./card-component.component.scss']
})
export class CardComponent implements OnInit {
  @Input() listOfImages: any;
  @Output() imageSelect = new EventEmitter<any>();

  httpClient: HttpClient;

  constructor(http: HttpClient) {
    this.httpClient = http;
   }

  ngOnInit(): void {
  }

  getBlobData(imageUrl: string): Observable<Blob> {
    return this.httpClient.get(imageUrl, { responseType: 'blob' });
  }

  // Get blob data from image url and download image as png with author name
  downloadImage(imageUrl: string, authorName: string): void {
    let blobData;
    this.getBlobData(imageUrl).subscribe(res => {
      if (res) {
        blobData = URL.createObjectURL(res);
        const anchorElement = document.createElement('a');
        anchorElement.href = blobData;
        anchorElement.download = authorName + '.png';
        anchorElement.style.display = 'none';
        document.body.appendChild(anchorElement);
        anchorElement.click();
      }
    });
  }

  // Open pop up to show user selected image
  openImage(imageUrl: string): void {
    this.imageSelect.emit(imageUrl);
  }
}

import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { constants } from '../../constants';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  domSanitizer: DomSanitizer;
  httpClient: HttpClient;

  availableImages: any;
  currentPage: number;
  nextLink: string;
  pageLimit: number;
  pageLimits: Array<number>;
  previousLink: string;
  randomImage: SafeUrl;
  requestUrl: string;
  selectedImageUrl: string;

  constructor(domSanitizer: DomSanitizer, httpClient: HttpClient) {
    this.domSanitizer = domSanitizer;
    this.httpClient = httpClient;

    this.availableImages = [];
    this.currentPage = 1;
    this.nextLink = '';
    this.pageLimit = 10;
    this.pageLimits = [];
    this.previousLink = '';
    this.randomImage = '';
    this.requestUrl = '';
    this.selectedImageUrl = '';
   }

  ngOnInit(): void {
    window.onresize = () => {
      this.loadRandomImage();
    };
    this.loadRandomImage();
    this.loadAvailableImages();
    this.getPageLimits();
  }

  changeLimit(): void {
    this.requestUrl = '';
    this.loadAvailableImages();
  }

  closeDialog(): void {
    this.selectedImageUrl = '';
  }

  formatImages(images: any): void {
    for (let i = 0; i < images.length; i++) {
      images[i].imageUrl = `${constants.api}id/${images[i].id}/250/200`;
    }
    this.availableImages = images;
  }

  // Generate page limits with interval 5
  getPageLimits(): void {
    for (let i = 10; i <= 50; i = i + 5) {
      this.pageLimits.push(i);
    }
  }

  loadAvailableImages(linkUrl?: string, pageNo: number = 0): void {
    this.currentPage += pageNo;
    this.availableImages = [];
    this.previousLink = '';
    this.nextLink = '';

    this.requestUrl = this.requestUrl || `${constants.api}v2/list?page=${this.currentPage}&limit=${this.pageLimit}`;
    this.requestUrl = linkUrl || this.requestUrl;

    this.httpClient.get(this.requestUrl, { observe: 'response' }).subscribe(res => {
      this.formatImages(res.body);
      const links = res.headers.get('link')?.split(',') || [];
      for (const link of links) {
        if (link.indexOf('prev') !== -1) {
          this.previousLink = link.split(';')[0].trim().slice(1, -1);
        }
        if (link.indexOf('next') !== -1) {
          this.nextLink = link.split(';')[0].trim().slice(1, -1);
        }
       }
    });
  }

  loadRandomImage(): void {
    this.randomImage = '';
    const endPoint = `${constants.api}${window.innerWidth - 20}/80`;
    this.httpClient.get(endPoint, {responseType: 'blob'}).subscribe(res => {
      this.randomImage = this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(res));
    });
  }

  openImage(event: string): void {
    this.selectedImageUrl = event;
  }

}

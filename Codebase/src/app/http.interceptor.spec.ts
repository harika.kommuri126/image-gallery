import { TestBed } from '@angular/core/testing';

import { MyHttpInterceptor } from './http.interceptor';

describe('HttpInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      MyHttpInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: MyHttpInterceptor = TestBed.inject(MyHttpInterceptor);
    expect(interceptor).toBeTruthy();
  });

//   it('adding header test', () => {

//     http.get('/data').subscribe(
//         response => {
//             expect(response).toBeTruthy();
//         }
//     );

//     expect(response.status).toEqual('401');
// }));
});

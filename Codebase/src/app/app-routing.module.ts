import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';


const dashboardRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  }
];

const pageNotFoundRoute: Routes = [
  {
    path: '404',
    component: PageNotFoundComponent
  }
];

const routes: Routes = [
  ...dashboardRoutes,
  ...pageNotFoundRoute,
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

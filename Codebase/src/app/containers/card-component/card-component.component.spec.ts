import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { CardComponent } from './card-component.component';
import { constants } from '../../constants';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  const listOfImages = [
    {
      id: '0',
      author: 'Alejandro Escamilla',
      width: 5616,
      height: 3744,
      url: constants.image_url,
      download_url: `${constants.api}id/0/5616/3744`
    }
  ];

  const imageUrl = 'https://i.picsum.photos/id/0/5616/3744.jpg?hmac=3GAAioiQziMGEtLbfrdbcoenXoWAW-zlyEAMkfEdBzQ';

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      declarations: [ CardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit clicked image url', () => {
    spyOn(component.imageSelect, 'emit').and.callThrough();
    component.openImage(constants.image_url);
    expect(component.imageSelect.emit).toHaveBeenCalledWith(constants.image_url);
  });

  it('should call getBlobData function', () => {
    spyOn(component, 'getBlobData').and.callThrough();
    component.downloadImage(imageUrl, 'Alejandro Escamilla');
    expect(component.getBlobData).toHaveBeenCalledWith(imageUrl);
  });
});

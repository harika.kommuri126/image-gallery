import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-image-container',
  templateUrl: './image-container.component.html',
  styleUrls: ['./image-container.component.scss']
})
export class ImageContainerComponent implements OnInit {
  @Input() imageUrl: string;
  @Output() isClosed: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.imageUrl = '';
  }

  ngOnInit(): void {
  }

  closeDialog(): void {
    this.isClosed.emit();
  }

}

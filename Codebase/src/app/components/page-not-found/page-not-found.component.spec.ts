import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from '../dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found.component';

describe('PageNotFoundComponent', () => {
  let component: PageNotFoundComponent;
  let fixture: ComponentFixture<PageNotFoundComponent>;
  let router: Router;
  const routes: Routes = [{ path: 'dashboard', component: DashboardComponent }];
  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterModule.forRoot(routes)],
      declarations: [ PageNotFoundComponent ],
      providers: [{ provide: Router, useValue: mockRouter }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotFoundComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to dashboard page', () => {
    component.goToDashboard();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['dashboard']);
  })
});

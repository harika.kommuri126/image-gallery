# image-gallary-app


### Project Description
```

A simple application to populate images with author name and open image or author details page when clicked on link. This application is developed in Angular.
```

### Technologies
```
Here you can see the tech stack used to develop this project
```

### Main Stack
```
- HTML5
- Sass
- ECMAScript 6 (ES6)
```

### Angular
I have choosen Angular Framework because it is easy to create single page applications and can achieve navigation between pages using routing.

Below I show you the packages I developed the application with:

- Angular cli (v11.0.2)
- HTTP client: HttpClient
- Unit testing: Karma
- Routes management: Angular Router
- Preprocessor: Sass (dart-sass)
- Code formatters: ES-Linter

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Unit test 
```
npm run test:unit
```